# Micro Continuous Integration / Deployment for Node / Git project

Zero-configuration single Bash script for automatically updating, building, testing and deploying
a Git / Node project.

The tool notifies you each time a new version of your project is successfully deployed, or fails. It looks for a new git commit each time the script is called (eg from a cron job), if no new commit exists then no more CPU cycles are wasted.

Suitable for mini web servers, React project, bots etc.

## Project requirements

The CD / CI expects a couple of npm commands to perform the actual tasks:

**npm build** (optional): Builds the projects, eg transpiling Javascript for React etc. Expected to return non-0 on failure

**npm test** (optional): Tests the projects, eg unit tests, integration test, etc. Expected to return non-0 on failure

**npm restart** (optional): Deploys and/or restarts the service, whatever applies to your project. Expected to return non-0 on failure

**npm notify <msg>** (required): Sends a message to project admin. Its up to you how to implement this, recommended is to send a bot message using Webex Teams or Slack.

If one of the optional scripts is not found in the npm package.json file, it is simply ignored.

See the example notify.js script for a simple way to notify using Webex Teams.

## Install

* Clone the repo

* Test the script: **./micro-ci.sh ../<your project folder>**

## Setup automatic task / cron job

Warning: a common problem with cron job is that it runs in a different environment than your usual bash command line. Typical problem is running a much older npm/node version. Fix this by finding out where your npm/node executable is and add to your path, similar to this:

```
#!/bin/bash

# need this to get correct node/npm version:
PATH=/usr/local/bin:$PATH

~/micro-ci/micro-ci.sh ~/mywebproject
```
