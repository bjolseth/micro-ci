#!/bin/bash

app=$0
dir=$1
branch=$2
package_json_changed=false
if [ -z $branch ]; then branch="master"; fi;

function usage () {
  echo "Syntax: $app <npm/git directory> <branch=master>"
  echo
  echo Updates, builds, tests and deploys a git/npm project
  echo
  echo The following scripts are supported:
  echo
  echo npm build - building the project
  echo npm test - running the project tests
  echo npm restart - restarting the service
  echo "npm notify <msg> - notify success / failure to admins"
  echo
  echo Scripts are expected to return non-zero value on failure
}

function checkUpdate () {
  # verify no local changes
  git status | grep "nothing to commit" > /dev/null
  if [ $? -ne 0 ]
  then
    echo "Project contains local uncommited changes"
    exit 1
  fi

  # update local repo
  echo "Fetching remote commits..."
  git fetch

  # get last commit in remote branch
  lastRemoteCommit=`git log -1 --pretty=format:"%h" origin/$branch`

  # get last commit local branch
  lastLocalCommit=`git log -1 --pretty=format:"%h" $branch`

  # echo $lastRemoteCommit, $lastLocalCommit

  # check if package.json has changed
  git diff $branch..origin/$branch -- package.json | grep diff
  if [ $? -eq 0 ]; then package_json_changed=true; fi;

  if [ $lastRemoteCommit == $lastLocalCommit ];
    then
      echo "No update needed"
      exit 0
  fi

  # save list of commits only in origin:
  diff=`git log --pretty=format:"%h: %s,"  origin/$branch ^$branch`

  echo Updating branch
  git pull --rebase
}

function build () {
  if [ "$package_json_changed"=true ];
  then
    echo "package.json updated, running npm install"
    npm install
    if [ $? -ne 0 ]
    then
      npm run notify "npm install failed"
      exit 1
    fi
  fi

  grep '"build":' package.json > /dev/null
  if [ $? -ne 0 ]; then echo "No build script, skipping"; return 0; fi

  npm run build
  if [ $? -ne 0 ]
  then
    echo "Build failed"
    npm run notify "😡 System build failed"
    exit 1
  fi
}

function test () {
  grep '"test":' package.json > /dev/null
  if [ $? -ne 0 ]; then echo "No test script, skipping"; return 0; fi

  npm run test
  if [ $? -ne 0 ]
  then
    echo test failed
    npm run notify "😡 System tests failed"
    exit 1
  fi
}

function deploy () {
  grep '"restart":' package.json > /dev/null

  if [ $? -ne 0 ]; then echo "No restart script, skipping"; return 0; fi

  npm run restart

  if [ $? -ne 0 ]
  then
    npm run notify "😡 System restart failed"
    exit 1
  fi
}

function notify () {
  npm run notify "🤩 System upgraded succesfully: $dir"
  npm run notify "Commits: $diff"
}

if [ $# -eq 0 ]; then usage; exit 1; fi

cd $dir
checkUpdate
build
test
deploy
notify
