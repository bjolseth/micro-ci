// Example script on how to notify admin on webex teams
// Requires the ciscospark module

const ciscospark = require('ciscospark');

const botToken = '...your token here';
const webexRoom = '... id for webex room here';

function sendMessage(text) {
  const webex = ciscospark.init({
    credentials: { access_token: botToken },
  });

  const msg = 'micro-CI says: ' + text;
  return webex.messages.create({
    roomId: webexRoom,
    markdown: msg,
  })
}

if (process.argv.length > 2) {
  const message = process.argv[2];
  sendMessage(message)
    .catch(() => process.exit(1));
}
